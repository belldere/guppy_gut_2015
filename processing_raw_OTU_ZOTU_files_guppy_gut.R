

library(phyloseq)
library(dplyr)
library(tidyr)
library(stringr)


#####Begin OTU based community####
#map file from Will's files
map_gup_gut=sample_data(read.delim("D:/EvansLab/Guppy_Gut_2015/16s_map_guppy015.txt", row.names = 1, header = T))
summary(map_gup_gut)
nrow(map_gup_gut)
#508

#load in the OTU table
otu_gup_gut=otu_table(read.table("D:/EvansLab/Guppy_Gut_2015/combined_16S_guppy_gut_015_OTU_table.txt",sep = "\t", header = T, row.names = 1), taxa_are_rows = T)
otu_gup_gut[1:10,1:10]
ntaxa(otu_gup_gut)
#36000

#Read in the Tree file created using PASTA https://github.com/smirarab/pasta

tree_otu_gup_gut=read_tree("D:/EvansLab/Guppy_Gut_2015/tree_OTU_guppy_gut_015.NWK")


#load in RDP taxon file. This is based off of RDP v16 (https://drive5.com/sintax/rdp_16s_v16.fa.gz)
taxa_raw_RDP_gup_gut= read.table("D:/EvansLab/Guppy_Gut_2015/Raw_USEARCH_files/combined_16S_guppy_gut_015_otus_RDP_v16_taxonomy.sintax",sep = "\t")
nrow(taxa_raw_RDP_gup_gut)
#36000
head(taxa_raw_RDP_gup_gut)

#Now subset so we only have classifications at 80% confidence
taxa_80C_RDP_gup_gut=taxa_raw_RDP_gup_gut[,c(1,4)]
head(taxa_80C_RDP_gup_gut)

#Seperate the taxa column so we have our taxa levels in columns
taxa_80C_RDP_gup_gut_sep=taxa_80C_RDP_gup_gut %>% separate(V4, c("Domain","Phylum","Class","Order","Family","Genus"),sep = ",")

#Make the OTU names the row names for the phyloseq obj creation
row.names(taxa_80C_RDP_gup_gut_sep)=taxa_80C_RDP_gup_gut_sep$V1
taxa_80C_RDP_gup_gut_sep$V1=NULL

#Now we need to get rid of the NA that were produced in the seperate step
#Call them UNKNOWN
taxa_80C_RDP_gup_gut_sep[is.na(taxa_80C_RDP_gup_gut_sep)] <- "UNKNOWN"
unique(taxa_80C_RDP_gup_gut_sep$Domain)
unique(taxa_80C_RDP_gup_gut_sep$Phylum)
unique(taxa_80C_RDP_gup_gut_sep$Class)
unique(taxa_80C_RDP_gup_gut_sep$Order)
unique(taxa_80C_RDP_gup_gut_sep$Genus)

#create the taxa file for the phylo obj
taxa_80C_RDP_gup_gut_sep_mat=as.matrix(taxa_80C_RDP_gup_gut_sep)
head(taxa_80C_RDP_gup_gut_sep_mat)
TAXA_80C_RDP_gup_gut=tax_table(taxa_80C_RDP_gup_gut_sep_mat)

#Output the processed taxa file for other uses
write.table(taxa_80C_RDP_gup_gut_sep_mat, file = "D:/EvansLab/Guppy_Gut_2015/taxa_80C_RDP_OTU_guppy_gut_2015.txt", sep = "\t")

#Combine the attributes to make the phyloseq obj with SILVA taxonomy
phyl_RDP_gup_gut=phyloseq(otu_gup_gut,map_gup_gut,TAXA_80C_RDP_gup_gut,tree_otu_gup_gut)
ntaxa(phyl_RDP_gup_gut)
#36000
sum(taxa_sums(phyl_RDP_gup_gut))
#11484021

#save the phylo obj
save(phyl_SILVA_gup_gut, file = "D:/EvansLab/Guppy_Gut_2015/OTU_RDP_phyloseq_obj.Rdata")

#load in GTDB based taxonomy (https://data.ace.uq.edu.au/public/gtdb/data/releases/release89/89.0/
#QIIME2 native bayesian classifier was used for this classification (https://docs.qiime2.org/2019.4/tutorials/feature-classifier/)

taxa_raw_GTDBr89_gup_gut= read.delim("D:/EvansLab/Guppy_Gut_2015/Raw_USEARCH_files/combined_16S_guppy_gut_015_otus_GTDBr89_taxonomy.tsv",sep = c("\t"),header = T)
head(taxa_raw_GTDBr89_gup_gut)

#Seperate the taxa column so we have our taxa levels in columns
taxa_raw_GTDBr89_gup_gut_sep=taxa_raw_GTDBr89_gup_gut %>% separate("Taxon", c("Domain","Phylum","Class","Order","Family","Genus","Species"),sep = ";")

#Make the OTU names the row names for the phyloseq obj creation
row.names(taxa_raw_GTDBr89_gup_gut_sep)=taxa_raw_GTDBr89_gup_gut_sep$Feature.ID
taxa_raw_GTDBr89_gup_gut_sep$Feature.ID=NULL

#Now we need to get rid of the NA that were produced in the seperate step
#Call them UNKNOWN
taxa_raw_GTDBr89_gup_gut_sep[is.na(taxa_raw_GTDBr89_gup_gut_sep)] <- "UNKNOWN"
head(taxa_raw_GTDBr89_gup_gut_sep)

#The NAtive Bayesian Classifier uses 70% as the default cuttoff for classification.
min(taxa_raw_GTDBr89_gup_gut_sep$Confidence)

#It looks like the GTDB did a poor job of classifying the dataset... I am not sure why.

unique(taxa_raw_GTDBr89_gup_gut_sep$Domain)
unique(taxa_raw_GTDBr89_gup_gut_sep$Phylum)
unique(taxa_raw_GTDBr89_gup_gut_sep$Class)
unique(taxa_raw_GTDBr89_gup_gut_sep$Order)
unique(taxa_raw_GTDBr89_gup_gut_sep$Genus)

#create the taxa file for the phylo obj
taxa_raw_GTDBr89_gup_gut_sep_mat=as.matrix(taxa_raw_GTDBr89_gup_gut_sep)
head(taxa_raw_GTDBr89_gup_gut_sep_mat)
TAXA_70c_GTDBr89_gup_gut=tax_table(taxa_raw_GTDBr89_gup_gut_sep_mat)

#Output the processed taxa file for other uses
write.table(taxa_raw_GTDBr89_gup_gut_sep_mat, file = "D:/EvansLab/Guppy_Gut_2015/taxa_70C_GTDBr89_OTU_guppy_gut_2015.txt", sep = "\t")

#Combine the attributes to make the phyloseq obj with SGTDBr89taxonomy
phyl_GTDBr89_gup_gut=phyloseq(otu_gup_gut,map_gup_gut,TAXA_70c_GTDBr89_gup_gut)
ntaxa(phyl_GTDBr89_gup_gut)
#36000
sum(taxa_sums(phyl_GTDBr89_gup_gut))
#11484021

#load in SILVA taxon file. This is based off of SILVA v 123 (https://drive5.com/sintax/silva_16s_v123.fa.gz)
taxa_raw_SILVA_gup_gut= read.delim("D:/EvansLab/Guppy_Gut_2015/Raw_USEARCH_files/combined_16S_guppy_gut_015_silva123_taxonomy.sintax",sep = c("\t"),header = F)
nrow(taxa_raw_SILVA_gup_gut)
#36000
head(taxa_raw_SILVA_gup_gut)

#Now subset so we only have classifications at 80% confidence
taxa_80C_SILVA_gup_gut=taxa_raw_SILVA_gup_gut[,c(1,4)]
head(taxa_80C_SILVA_gup_gut)

#Seperate the taxa column so we have our taxa levels in columns
taxa_80C_SILVA_gup_gut_sep=taxa_80C_SILVA_gup_gut %>% separate(V4, c("Domain","Phylum","Class","Order","Family","Genus","Species"),sep = ",")

#Make the OTU names the row names for the phyloseq obj creation
row.names(taxa_80C_SILVA_gup_gut_sep)=taxa_80C_SILVA_gup_gut_sep$V1
taxa_80C_SILVA_gup_gut_sep$V1=NULL

#Now we need to get rid of the NA that were produced in the seperate step
#Call them UNKNOWN
taxa_80C_SILVA_gup_gut_sep[is.na(taxa_80C_SILVA_gup_gut_sep)] <- "UNKNOWN"

#create the taxa file for the phylo obj
taxa_80C_SILVA_gup_gut_sep_mat=as.matrix(taxa_80C_SILVA_gup_gut_sep)
head(taxa_80C_SILVA_gup_gut_sep_mat)
TAXA_80C_SILVA_gup_gut=tax_table(taxa_80C_SILVA_gup_gut_sep_mat)

#Output the processed taxa file for other uses
write.table(taxa_80C_SILVA_gup_gut_sep_mat, file = "D:/EvansLab/Guppy_Gut_2015/taxa_80C_SILVA123_OTU_guppy_gut_2015.txt", sep = "\t")

#Combine the attributes to make the phyloseq obj with SILVA taxonomy
phyl_SILVA_gup_gut=phyloseq(otu_gup_gut,map_gup_gut,TAXA_80C_SILVA_gup_gut,tree_otu_gup_gut)
ntaxa(phyl_SILVA_gup_gut)
#36000
sum(taxa_sums(phyl_SILVA_gup_gut))
#11484021
sort(sample_sums(phyl_SILVA_gup_gut))
mean(sample_sums(phyl_SILVA_gup_gut))

#save the phylo obj
save(phyl_SILVA_gup_gut, file = "D:/EvansLab/Guppy_Gut_2015/OTU_SILVA123_phyloseq_obj.Rdata")

#####End OTU based community####



#####Begin ZOTU based community####
#map file from Will's files
map_gup_gut=sample_data(read.delim("D:/EvansLab/Guppy_Gut_2015/16s_map_guppy015.txt", row.names = 1, header = T))
summary(map_gup_gut)
nrow(map_gup_gut)
#508

#load in the OTU table
Zotu_gup_gut=otu_table(read.table("D:/EvansLab/Guppy_Gut_2015/combined_16S_guppy_gut_015_ZOTUs_table.txt",sep = "\t", header = T, row.names = 1), taxa_are_rows = T)
Zotu_gup_gut[1:10,1:10]
ntaxa(Zotu_gup_gut)
#109891

#Read in the Tree file created using PASTA https://github.com/smirarab/pasta

tree_Zotu_gup_gut=read_tree("D:/EvansLab/Guppy_Gut_2015/tree_ZOTUs_guppy_gut_015.NWK")

#load in RDP taxon file. This is based off of RDP v16 (https://drive5.com/sintax/rdp_16s_v16.fa.gz)
ZOTU_taxa_raw_RDP_gup_gut= read.table("D:/EvansLab/Guppy_Gut_2015/Raw_USEARCH_files/combined_16S_guppy_gut_015_ZOTUs_RDP_v16_taxonomy.sintax",sep = "\t")
nrow(ZOTU_taxa_raw_RDP_gup_gut)
#109891
head(ZOTU_taxa_raw_RDP_gup_gut)

#Now subset so we only have classifications at 80% confidence
ZOTU_taxa_80C_RDP_gup_gut=ZOTU_taxa_raw_RDP_gup_gut[,c(1,4)]
head(ZOTU_taxa_80C_RDP_gup_gut)

#Seperate the taxa column so we have our taxa levels in columns
ZOTU_taxa_80C_RDP_gup_gut_sep=ZOTU_taxa_80C_RDP_gup_gut %>% separate(V4, c("Domain","Phylum","Class","Order","Family","Genus"),sep = ",")

#Make the OTU names the row names for the phyloseq obj creation
row.names(ZOTU_taxa_80C_RDP_gup_gut_sep)=ZOTU_taxa_80C_RDP_gup_gut_sep$V1
ZOTU_taxa_80C_RDP_gup_gut_sep$V1=NULL

#Now we need to get rid of the NA that were produced in the seperate step
#Call them UNKNOWN
ZOTU_taxa_80C_RDP_gup_gut_sep[is.na(ZOTU_taxa_80C_RDP_gup_gut_sep)] <- "UNKNOWN"
unique(ZOTU_taxa_80C_RDP_gup_gut_sep$Domain)
unique(ZOTU_taxa_80C_RDP_gup_gut_sep$Phylum)
unique(ZOTU_taxa_80C_RDP_gup_gut_sep$Class)
unique(ZOTU_taxa_80C_RDP_gup_gut_sep$Order)
unique(ZOTU_taxa_80C_RDP_gup_gut_sep$Genus)

#create the taxa file for the phylo obj
ZOTU_taxa_80C_RDP_gup_gut_sep_mat=as.matrix(ZOTU_taxa_80C_RDP_gup_gut_sep)
head(ZOTU_taxa_80C_RDP_gup_gut_sep_mat)
ZOTU_TAXA_80C_RDP_gup_gut=tax_table(ZOTU_taxa_80C_RDP_gup_gut_sep_mat)

#Output the processed taxa file for other uses
write.table(ZOTU_taxa_80C_RDP_gup_gut_sep_mat, file = "D:/EvansLab/Guppy_Gut_2015/taxa_80C_RDP_ZOTU_guppy_gut_2015.txt", sep = "\t")

#Combine the attributes to make the phyloseq obj with RDP taxonomy
phyl_ZOTU_RDP_gup_gut=phyloseq(Zotu_gup_gut,map_gup_gut,ZOTU_TAXA_80C_RDP_gup_gut,tree_Zotu_gup_gut)
ntaxa(phyl_ZOTU_RDP_gup_gut)
#109891
sum(taxa_sums(phyl_ZOTU_RDP_gup_gut))
#11886134

save(phyl_ZOTU_RDP_gup_gut, file = "D:/EvansLab/Guppy_Gut_2015/ZOTU_RDP_phyloseq_obj.Rdata")

#load in GTDB based taxonomy (https://data.ace.uq.edu.au/public/gtdb/data/releases/release89/89.0/
#QIIME2 native bayesian classifier was used for this classification (https://docs.qiime2.org/2019.4/tutorials/feature-classifier/)

ZOTU_taxa_raw_GTDBr89_gup_gut= read.delim("D:/EvansLab/Guppy_Gut_2015/Raw_USEARCH_files/combined_16S_guppy_gut_015_ZOTUs_GTDBr89_taxonomy.tsv",sep = c("\t"),header = T)
head(ZOTU_taxa_raw_GTDBr89_gup_gut)

#Seperate the taxa column so we have our taxa levels in columns
ZOTU_taxa_raw_GTDBr89_gup_gut_sep=ZOTU_taxa_raw_GTDBr89_gup_gut %>% separate("Taxon", c("Domain","Phylum","Class","Order","Family","Genus","Species"),sep = ";")

#need to rename the ZOTUs because case does not match the ZOTU table 
ZOTU_taxa_raw_GTDBr89_gup_gut_sep$Feature.ID=str_replace(ZOTU_taxa_raw_GTDBr89_gup_gut_sep$Feature.ID,"ZOTU","Zotu")

#Make the OTU names the row names for the phyloseq obj creation
row.names(ZOTU_taxa_raw_GTDBr89_gup_gut_sep)=ZOTU_taxa_raw_GTDBr89_gup_gut_sep$Feature.ID
ZOTU_taxa_raw_GTDBr89_gup_gut_sep$Feature.ID=NULL

#Now we need to get rid of the NA that were produced in the seperate step
#Call them UNKNOWN
ZOTU_taxa_raw_GTDBr89_gup_gut_sep[is.na(ZOTU_taxa_raw_GTDBr89_gup_gut_sep)] <- "UNKNOWN"
head(ZOTU_taxa_raw_GTDBr89_gup_gut_sep)

#The NAtive Bayesian Classifier uses 70% as the default cuttoff for classification.
min(ZOTU_taxa_raw_GTDBr89_gup_gut_sep$Confidence)

#It looks like the GTDB did a poor job of classifying the dataset... I am not sure why.

unique(ZOTU_taxa_raw_GTDBr89_gup_gut_sep$Domain)
unique(ZOTU_taxa_raw_GTDBr89_gup_gut_sep$Phylum)
unique(ZOTU_taxa_raw_GTDBr89_gup_gut_sep$Class)
unique(ZOTU_taxa_raw_GTDBr89_gup_gut_sep$Order)
unique(ZOTU_taxa_raw_GTDBr89_gup_gut_sep$Genus)




#create the taxa file for the phylo obj
ZOTU_taxa_raw_GTDBr89_gup_gut_sep_mat=as.matrix(ZOTU_taxa_raw_GTDBr89_gup_gut_sep)
head(ZOTU_taxa_raw_GTDBr89_gup_gut_sep_mat)
ZOTU_TAXA_70c_GTDBr89_gup_gut=tax_table(ZOTU_taxa_raw_GTDBr89_gup_gut_sep_mat)

#Output the processed taxa file for other uses
write.table(ZOTU_taxa_raw_GTDBr89_gup_gut_sep_mat, file = "D:/EvansLab/Guppy_Gut_2015/taxa_70C_GTDBr89_ZOTU_guppy_gut_2015.txt", sep = "\t")

#Combine the attributes to make the phyloseq obj with SGTDBr89taxonomy
phyl_ZOTU_GTDBr89_gup_gut=phyloseq(Zotu_gup_gut,map_gup_gut,ZOTU_TAXA_70c_GTDBr89_gup_gut)
ntaxa(phyl_ZOTU_GTDBr89_gup_gut)
#109891
sum(taxa_sums(phyl_ZOTU_GTDBr89_gup_gut))
#11886134

#load in SILVA taxon file. This is based off of SILVA v 123 (https://drive5.com/sintax/silva_16s_v123.fa.gz)
ZOTU_taxa_raw_SILVA_gup_gut= read.delim("D:/EvansLab/Guppy_Gut_2015/Raw_USEARCH_files/combined_16S_guppy_gut_015_ZOTUs_silva123_taxonomy.sintax",sep = c("\t"),header = F)
nrow(ZOTU_taxa_raw_SILVA_gup_gut)
#109891
head(ZOTU_taxa_raw_SILVA_gup_gut)

#Now subset so we only have classifications at 80% confidence
ZOTU_taxa_80C_SILVA_gup_gut=ZOTU_taxa_raw_SILVA_gup_gut[,c(1,4)]
head(ZOTU_taxa_80C_SILVA_gup_gut)

#Seperate the taxa column so we have our taxa levels in columns
ZOTU_taxa_80C_SILVA_gup_gut_sep=ZOTU_taxa_80C_SILVA_gup_gut %>% separate(V4, c("Domain","Phylum","Class","Order","Family","Genus","Species"),sep = ",")

#Make the OTU names the row names for the phyloseq obj creation
row.names(ZOTU_taxa_80C_SILVA_gup_gut_sep)=ZOTU_taxa_80C_SILVA_gup_gut_sep$V1
ZOTU_taxa_80C_SILVA_gup_gut_sep$V1=NULL

#Now we need to get rid of the NA that were produced in the seperate step
#Call them UNKNOWN
ZOTU_taxa_80C_SILVA_gup_gut_sep[is.na(ZOTU_taxa_80C_SILVA_gup_gut_sep)] <- "UNKNOWN"

#create the taxa file for the phylo obj
ZOTU_taxa_80C_SILVA_gup_gut_sep_mat=as.matrix(ZOTU_taxa_80C_SILVA_gup_gut_sep)
head(ZOTU_taxa_80C_SILVA_gup_gut_sep_mat)
ZOTU_TAXA_80C_SILVA_gup_gut=tax_table(ZOTU_taxa_80C_SILVA_gup_gut_sep_mat)

#Output the processed taxa file for other uses
write.table(ZOTU_taxa_80C_SILVA_gup_gut_sep_mat, file = "D:/EvansLab/Guppy_Gut_2015/taxa_80C_SILVA123_ZOTU_guppy_gut_2015.txt", sep = "\t")

#Combine the attributes to make the phyloseq obj with SILVA taxonomy
phyl_ZOTU_SILVA_gup_gut=phyloseq(Zotu_gup_gut,map_gup_gut,ZOTU_TAXA_80C_SILVA_gup_gut,tree_Zotu_gup_gut)
ntaxa(phyl_ZOTU_SILVA_gup_gut)
#109891
sum(taxa_sums(phyl_ZOTU_SILVA_gup_gut))
#11886134

sort(sample_sums(phyl_ZOTU_SILVA_gup_gut))
mean(sample_sums(phyl_ZOTU_SILVA_gup_gut))

save(phyl_SILVA_gup_gut, file = "D:/EvansLab/Guppy_Gut_2015/ZOTU_SILVA123_phyloseq_obj.Rdata")
#####End ZOTU based community####